const container = document.querySelector('.carousel__container');
const slides = Array.from(container.children);
const prevSlideButton = document.querySelector('.carousel__left');
const nextSlideButton = document.querySelector('.carousel__right');
let currentIndex = 0; 

// get slide's width
//let slideWidth = slides[0].getBoundingClientRect().width;
let slideWidth = window.innerWidth;

// set each slide's left position
const slidePosition = (slide,index) => {
    slide.style.left = slideWidth * index + 'px';
}
slides.forEach(slidePosition);

// move container's left position depending on which slide is to be displayed
const moveToSlide = (container, currentSlide, targetSlide) => {
    container.style.transform = 'translateX(-' + targetSlide.style.left + ')';
    currentSlide.classList.remove('current-slide');
    targetSlide.classList.add('current-slide');

    // hide left arrow button if displaying first slide
    if(currentIndex == 0){
        prevSlideButton.classList.add('hidden');
    } else {
        prevSlideButton.classList.remove('hidden');
    }

    // hide left arrow button if displaying last slide
    if(currentIndex == slides.length-1){
        nextSlideButton.classList.add('hidden');
    } else {
        nextSlideButton.classList.remove('hidden');
    }
}

const reportWindowSize = () => {
  slideWidth = window.innerWidth;
  slides.forEach(slidePosition);
  const currentSlide = container.querySelector('.current-slide');
  moveToSlide(container, currentSlide, currentSlide);
}

window.onresize = reportWindowSize;

// when left arrow button is clicked
prevSlideButton.addEventListener('click', e=>{
    if(currentIndex > 0) {
        const currentSlide = container.querySelector('.current-slide');
        const prevSlide = currentSlide.previousElementSibling;
        currentIndex--;
        moveToSlide(container, currentSlide, prevSlide);
    }
})

// when right arrow button is clicked
nextSlideButton.addEventListener('click', e=>{
    if(currentIndex < slides.length-1) {
        const currentSlide = container.querySelector('.current-slide');
        const nextSlide = currentSlide.nextElementSibling;
        currentIndex++;
        moveToSlide(container, currentSlide, nextSlide);
    }
})